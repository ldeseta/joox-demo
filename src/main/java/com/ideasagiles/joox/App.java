/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.joox;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.xml.sax.SAXException;

public class App {

    private static InputStream getDocument() throws FileNotFoundException {
//        return new FileInputStream("target/classes/modelos-1gb.xml");
        return new FileInputStream("src/main/resources/modelos-1gb.xml");
    }

    public static void main(String[] args) throws SAXException, IOException {
        EquipoParser equipoParser = new EquipoParser(getDocument());
        List<Equipo> equipos = equipoParser.parse();

        for (Equipo equipo : equipos) {
            System.out.println(equipo.toString());
            System.out.println("==========================================================\n\n");
        }
    }

}
