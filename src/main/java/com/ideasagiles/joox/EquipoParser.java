/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.joox;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.joox.JOOX.$;
import org.joox.Match;
import org.xml.sax.SAXException;

/**
 *
 * @author ldeseta
 */
public class EquipoParser {

    private InputStream document;

    public EquipoParser(InputStream document) {
        this.document = document;
    }

    public List<Equipo> parse() throws SAXException, IOException {
        List<Equipo> equipos = new ArrayList<>();
        List<Match> equiposMatch = $(document).find("SimpleProductSpecList > SimpleProductSpec").each();
        for (Match equipoMatch : equiposMatch) {
            String modelo = equipoMatch.find("Name > Value[locale='es']").content();
            Map<String, String> caracteristicas = getCaracteristicas(equipoMatch);
            Map<String, String> attachments = getAttachments(equipoMatch);

            Equipo equipo = new Equipo();
            equipo.setModelo(modelo);
            equipo.setCaracteristicas(caracteristicas);
            equipo.setAttachments(attachments);

            equipos.add(equipo);
        }
        return equipos;
    }

    private Map<String, String> getCaracteristicas(Match equipo) {
        Map<String, String> caracteristicas = new HashMap<>();

        List<Match> each = equipo.find("AssignableAttribute > Name > Value[locale='es']").each();
        for (Match match : each) {
            String caracteristica = match.content();
            String valor = match.parent().parent().parent().find("DefaultValue").attr("value");
            caracteristicas.put(caracteristica, valor);
        }
        return caracteristicas;
    }

    private Map<String, String> getAttachments(Match equipo) {
        Map<String, String> attachments = new HashMap<>();

        List<Match> each = equipo.find("SimpleProductSpecVersion > Attachment > Attachment").each();
        for (Match match : each) {
            String type = match.find("Type").attr("value");
            String value = match.find("Link").attr("value");
            attachments.put(type, value);
        }
        return attachments;
    }


}
