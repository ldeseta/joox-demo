/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.joox;

import java.util.Map;

/**
 *
 * @author ldeseta
 */
public class Equipo {

    private String modelo;
    private Map<String,String> caracteristicas;
    private Map<String,String> attachments;

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Map<String, String> getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(Map<String, String> caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public Map<String, String> getAttachments() {
        return attachments;
    }

    public void setAttachments(Map<String, String> attachments) {
        this.attachments = attachments;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Modelo: ").append(getModelo());
        for (Map.Entry<String, String> entry : caracteristicas.entrySet()) {
            String caracteristica = entry.getKey();
            String valor = entry.getValue();
            sb.append(caracteristica).append(": ").append(valor);
            sb.append("\n");
        }
        for (Map.Entry<String, String> entry : attachments.entrySet()) {
            String attachmentType = entry.getKey();
            String valor = entry.getValue();
            sb.append("ATTACHMENT ").append(attachmentType).append(": ").append(valor);
            sb.append("\n");
        }

        return sb.toString();
    }

}
