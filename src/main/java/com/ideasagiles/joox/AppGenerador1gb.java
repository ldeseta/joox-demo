/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ideasagiles.joox;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author ldeseta
 */
public class AppGenerador1gb {
    private static String getDocumentPart() throws IOException {
        return FileUtils.readFileToString(new File("target/classes/modelos-parte.xml"));
    }

    public static void main(String[] args) throws IOException {
        String part = getDocumentPart();

        File outputFile = new File("target/classes/modelos-1gb.xml");
        outputFile.delete();

        try (FileWriter fw = new FileWriter(outputFile)) {
            fw.write("<SimpleProductSpecList noNamespaceSchemaLocation=\"../../Catalog.xsd\" >\n <Id>d783aba6-1323-4e65-be84-7d14f66b8e42</Id>");
            for (int i=0; i<500; i++) {
                System.out.println("Concatenando... " + i);
                fw.write(part);
            }
            fw.write("</SimpleProductSpecList>");
        }
    }
}
